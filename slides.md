--- 
title: Seminar Magang
theme: default
layout: image
image: /assets/cover.jpg
class: 'text-center'
highlighter: shiki
selectable: true
download: true
info: |
  ## Seminar Magang
  
  OTOMATISASI SISTEM PEMBACAAN DATA TRANSAKSI PADA PT. BANK CENTRAL ASIA, Tbk (BCA)
transition: slide-left
---

<!-- Page 1 -->
---
src: /pages/1.md
---

<!-- Page 2 -->
---
src: /pages/2.md
---

<!-- Page 3 -->
---
src: /pages/3.md
---

<!-- Page 4 -->
---
src: /pages/4.md
---

<!-- Page 5 -->
---
src: /pages/5.md
---

<!-- Page 6 -->
---
src: /pages/6.md
---

<!-- Page 7 -->
---
src: /pages/7.md
---

<!-- Page 8 -->
---
src: /pages/8.md
---

<!-- Page 9 -->
---
src: /pages/9.md
---

<!-- Page 10 -->
---
src: /pages/10.md
---

<!-- Page 11 -->
---
src: /pages/11.md
---
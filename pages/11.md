---
layout: image
image: /assets/template.jpg
---

<style>
    .center {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
</style>

<img src="/assets/herta.gif" width="200" class="center">
<br>
<h1 style="color: black; text-align: center;">Terima Kasih!</h1>
<div style="display: flex; align-items: center; justify-content: center;">
    <div style="color: black;" class="opacity-50 text-sm mr-2">Powered by</div>
    <img class="w-5 h-5" src="/assets/slidev.png" alt="Slidev">
    <div style="color: rgb(32, 130, 166);"><b>Sli</b>dev </div>
</div>"